from django.urls import path
from .views import *

urlpatterns = [
	path('home/', homepage_view, name='home'),
	path('favGames/', favGames_view, name='favGames'),
	path('form/', formpage_view, name='form'),
	path('', homepage_view, name='')
]
