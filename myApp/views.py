from django.shortcuts import render

# Create your views here.

def homepage_view(request):
    return render(request, "Home.html")

def favGames_view(request):
    return render(request, "FavGames.html")

def formpage_view(request):
    return render(request, "Form.html")